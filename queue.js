let collection = [];

// Write the queue functions below.
//USAGE OF ARRAY METHODS(push, pop, shift, unshift, etc) is not allowed

//-------------------------------------------------------------
//(output all the elements of the queue)
function print() {
    return collection;    
}
//-------------------------------------------------------------
//add element to rear/last part of the queue
function enqueue(element) {
   collection[collection.length] = element;
   return collection;
}
//-------------------------------------------------------------
//remove element at front of queue
function dequeue() {
    for(i = 0; i < collection.length-1; i++) {
        collection[i] = collection[i += 1];
    }
    --collection.length
    return collection;
}
//-------------------------------------------------------------
//show element at the front
function front() {
    return collection[0]
}
//-------------------------------------------------------------
//show total number of elements
function size() {
  return collection.length
}
//-------------------------------------------------------------
//outputs Boolean value describing whether queue is empty of not
function isEmpty() {
    return collection.length === 0; 
    
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};